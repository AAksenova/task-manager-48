package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IUserEndpoint;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.marker.IntegrationCategory;
import ru.t1.aksenova.tm.service.PropertyService;

import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public class AuthEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpointClient.login(loginRequest).getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD);
        request.setEmail(USER_EMAIL);
        userEndpointClient.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken);
        requestRemove.setLogin(USER_LOGIN);
        userEndpointClient.removeUser(requestRemove);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(adminToken);
        authEndpointClient.logout(requestLogout);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD);
        return authEndpointClient.login(request).getToken();
    }

    @Test
    public void login() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(token);
        Assert.assertNotNull(authEndpointClient.viewProfile(request).getUser());
    }

    @Test
    public void logout() {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        authEndpointClient.logout(request);
        @NotNull final UserViewProfileRequest userProfileRequest = new UserViewProfileRequest(token);
        Assert.assertThrows(Exception.class, () -> authEndpointClient.viewProfile(userProfileRequest));
    }

    @Test
    public void viewProfile() {
        @Nullable final String token = getUserToken();
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(token);
        Assert.assertNotNull(authEndpointClient.viewProfile(request).getUser());
    }

}
