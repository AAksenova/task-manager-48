package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.marker.IntegrationCategory;
import ru.t1.aksenova.tm.service.PropertyService;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_PASSWORD;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String token;

    @Nullable
    private static ProjectDTO projectOne = new ProjectDTO();

    @Nullable
    private static ProjectDTO projectTwo = new ProjectDTO();

    private static int projectOneIndex;

    private static int projectTwoIndex;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        token = authEndpointClient.login(loginRequest).getToken();
        projectOne = createTestProject(ONE_PROJECT_NAME, ONE_PROJECT_DESCRIPTION);
        projectOneIndex = 0;
        projectTwo = createTestProject(TWO_PROJECT_NAME, TWO_PROJECT_DESCRIPTION);
        projectTwoIndex = 1;
    }

    public static ProjectDTO createTestProject(@NotNull final String name, @NotNull final String description) {
        @NotNull ProjectCreateRequest requestCreate = new ProjectCreateRequest(token);
        requestCreate.setName(name);
        requestCreate.setDescription(description);
        return projectEndpointClient.createProject(requestCreate).getProject();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull ProjectClearRequest requestClear = new ProjectClearRequest(token);
        projectEndpointClient.clearProject(requestClear);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        authEndpointClient.logout(requestLogout);
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(token);
        request.setId(projectOne.getId());
        request.setStatus(STATUS_IN_PROGRESS);
        Assert.assertNotNull(projectEndpointClient.changeProjectStatusById(request));
    }

    @Test
    public void clearProject() {
        Assert.assertNotNull(token);
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(token);
        Assert.assertNotNull(projectEndpointClient.clearProject(request));
        projectOne = createTestProject(ONE_PROJECT_NAME, ONE_PROJECT_DESCRIPTION);
        projectOneIndex = 0;
        projectTwo = createTestProject(TWO_PROJECT_NAME, TWO_PROJECT_DESCRIPTION);
        projectTwoIndex = 1;
    }

    @Test
    public void completeProjectById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(token);
        request.setId(projectOne.getId());
        Assert.assertNotNull(projectEndpointClient.completeProjectById(request));
    }

    @Test
    public void createProject() {
        Assert.assertNotNull(token);
        @NotNull ProjectCreateRequest requestCreate = new ProjectCreateRequest(token);
        requestCreate.setName(THREE_PROJECT_NAME);
        requestCreate.setDescription(THREE_PROJECT_DESCRIPTION);
        @Nullable ProjectDTO project = projectEndpointClient.createProject(requestCreate).getProject();
        Assert.assertNotNull(project);

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(token);
        request.setId(project.getId());
        Assert.assertNotNull(projectEndpointClient.removeProjectById(request));
    }

    @Ignore
    @Test
    public void listProject() {
        Assert.assertNotNull(token);
        @NotNull final ProjectListRequest request = new ProjectListRequest(token);
        request.setSort("BY_CREATED");
        Assert.assertNotNull(projectEndpointClient.listProject(request));
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(token);
        request.setId(projectTwo.getId());
        Assert.assertNotNull(projectEndpointClient.removeProjectById(request));

        projectTwo = createTestProject(TWO_PROJECT_NAME, TWO_PROJECT_DESCRIPTION);
        projectTwoIndex = 1;
    }

    @Test
    public void showProjectById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(token);
        request.setId(projectOne.getId());
        Assert.assertNotNull(projectEndpointClient.showProjectById(request));
    }


    @Test
    public void startProjectById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(token);
        request.setId(projectOne.getId());
        Assert.assertNotNull(projectEndpointClient.startProjectById(request));
    }


    @Test
    public void updateProjectById() {
        Assert.assertNotNull(token);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(token);
        request.setId(projectOne.getId());
        request.setDescription(ONE_PROJECT_DESCRIPTION + "update");
        request.setName(ONE_PROJECT_NAME + " update");
        Assert.assertNotNull(projectEndpointClient.updateProjectById(request));
    }

}
