package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IUserEndpoint;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.marker.IntegrationCategory;
import ru.t1.aksenova.tm.service.PropertyService;

import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public class UserEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpointClient.login(loginRequest).getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD);
        request.setEmail(USER_EMAIL);
        userEndpointClient.registryUser(request);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_LOGIN);
        userEndpointClient.removeUser(request);
    }

    private String getUserToken() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USER_LOGIN);
        request.setPassword(USER_PASSWORD);
        return authEndpointClient.login(request).getToken();
    }

    @Test
    public void changeUserPassword() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword(UPDATE_PASSWORD);
        Assert.assertNotNull(userEndpointClient.changeUserPassword(request));
        request.setPassword(USER_PASSWORD);
        Assert.assertNotNull(userEndpointClient.changeUserPassword(request));
    }

    @Test
    public void lockUser() {
        Assert.assertNotNull(adminToken);
        @NotNull final UserLockRequest request = new UserLockRequest(adminToken);
        request.setLogin(USER_LOGIN);
        Assert.assertNotNull(userEndpointClient.lockUser(request));
    }

    @Test
    public void registryUser() {
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken);
        requestRemove.setLogin(TEST_LOGIN);
        Assert.assertNotNull(userEndpointClient.removeUser(requestRemove));

        Assert.assertNotNull(adminToken);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        Assert.assertNotNull(userEndpointClient.registryUser(request));
    }

    @Test
    public void removeUser() {
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(adminToken);
        requestRemove.setLogin(TEST_LOGIN);
        Assert.assertNotNull(userEndpointClient.removeUser(requestRemove));

        Assert.assertNotNull(adminToken);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        Assert.assertNotNull(userEndpointClient.registryUser(request));
    }

    @Test
    public void unlockUser() {
        Assert.assertNotNull(adminToken);
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(adminToken);
        request.setLogin(USER_LOGIN);
        Assert.assertNotNull(userEndpointClient.unlockUser(request));
    }

    @Test
    public void updateUserProfile() {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setLastName(USER_LAST_NAME);
        request.setFirstName(USER_FIRST_NAME);
        request.setMiddleName(USER_MIDDLE_NAME);
        Assert.assertNotNull(userEndpointClient.updateUserProfile(request));
    }

}
