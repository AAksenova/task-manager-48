package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.service.PropertyService;

import java.util.UUID;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static UserDTO USER_TEST = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN_TEST = new UserDTO();

    @NotNull
    public final static String USER_TEST_LOGIN = "test2";

    @NotNull
    public final static String USER_TEST_PASSWORD = "test2";

    @NotNull
    public final static String USER_TEST_EMAIL = "test2@test.com";

    @NotNull
    public final static String USER_ADMIN_LOGIN = "admin2";

    @NotNull
    public final static String USER_ADMIN_PASSWORD = "admin2";

    @NotNull
    public final static String USER_ADMIN_EMAIL = "admin2@test.com";

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    public final static String NON_EXISTING_LOGIN = "LoginNoExist";

    @NotNull
    public final static String NON_EXISTING_EMAIL = "EmailNoExist@test.com";

    @NotNull
    public final static String NON_EXISTING_PASSWORD = "LoginNoExist";

    @NotNull
    public final static String FIRST_NAME = "Test First Name";

    @NotNull
    public final static String LAST_NAME = "Test Last Name";

    @NotNull
    public final static String MIDDLE_NAME = "Test Middle Name";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(USER_TEST_PASSWORD);
        USER_TEST.setPasswordHash(USER_TEST_PASSWORD);
        USER_TEST.setEmail(USER_TEST_EMAIL);

        ADMIN_TEST.setLogin(USER_ADMIN_LOGIN);
        ADMIN_TEST.setPasswordHash(USER_ADMIN_PASSWORD);
        ADMIN_TEST.setPasswordHash(USER_ADMIN_PASSWORD);
        ADMIN_TEST.setEmail(USER_ADMIN_EMAIL);
    }

}
