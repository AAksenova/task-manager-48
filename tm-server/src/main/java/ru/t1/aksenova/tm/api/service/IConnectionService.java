package ru.t1.aksenova.tm.api.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IConnectionService {

    @NotNull
    @SneakyThrows
    EntityManager getEntityManager();

    void close();

    void closeConnectionLiquibase() throws Exception;

    void getConnectionLiquibase() throws Exception;

    Liquibase liquibase(@NotNull String filename);
}
