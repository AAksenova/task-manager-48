package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;

public interface IProjectTaskDTOService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void unbindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void removeTaskToProject(
            @Nullable String userId,
            @Nullable String projectId
    );

}
